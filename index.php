<?php
// basico do basico

//muito util para debug identificar erro no codigo
ini_set('display_errors', 1);

ini_set('display_startup_errors', 1);

error_reporting(E_ALL);
//FIM nao usar em produção

//comentário de uma linha

 /*
Comentários 
de
bloco
*/

$nome = 'Tamires de Sa'; //uma variavel em PHP

echo 'Olá, ' . $nome . '!! <br><br>'; // concatena com ponto

echo "olá, $nome!"; //pode usar aspas duplas e simples

if ($nome == 'Tamires de Sa'){
    echo '<br><br> O nome é igual';

}else{

    echo '<br><br> O Nome não é igual';

}

echo '<br><br>';


echo "CONDICIONAIS<br>"; //CONDICIONAIS

if ($nome == 'Tamires de Sa'){
    
    echo '<br><br> O nome é igual <br>';

}else{

    echo '<br><br> O Nome não é igual';

}

echo "SWITCH<br>";

$dia = 'sexta';

switch($dia){

    case 'segunda';

    echo 'estude';       
    break;

echo '<br><br>';

case 'terça';

     echo 'aula de CMS';       
break;

case 'quarta';

        echo 'Aula de BD';       
break;

case 'quinta';

        echo 'seja feliz aqui e agora';       
break;

case 'sexta';

        echo 'faça outra coisa';       
break;

default:

    echo 'Va descansar';
}

echo '<br><br>';


//if ternário 

$animal = 'cachorro';

$tipo = $animal == 'cachorro' ? 'manifero' : 'desconhecido';

$sobrenome = $sobrenome_informado ?? "não informado"; // ?? indica se existe a variavel ou nao

echo  "<br> sobrenome: $sobrenome<br>";

echo "<br>$animal é um animal do tipo: $tipo<br>";

echo '<br><br>';

// loopings

echo "FOR<br>";

for($i = 0; $i < 10; $i++){

    echo "Essa é uma linha $i<br>";
}

$i = 0;

echo "WHILE<br>";

while( $i < 10 ){

    echo "Essa é uma linha $i<br>";

    $i++;
}

echo "DO / WHILE<br>";
$i = 0;

do{

    echo "Essa é uma linha $i<br>";

    $i++;
}while( $i < 10);

echo '<br> 2 + 2 = ' . (2+2) . '!';


// COMO CHAMAR OUTROS CODIGOS

include  'link.html'; //nome do arquivo colocar dentro das aspas , se não encontra o arquivo ele dá erro mas continua executando o codigo

require  'link.html'; // se não achar, da erro fatal

include_once 'link.html'; //verifica se ja foi incluido antes, se sim, nao inclui novamente

require_once  'link.html'; //verifica se ja foi incluido antes, se sim, nao inclui novamente
