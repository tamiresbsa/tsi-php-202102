<?php

//muito util para debug identificar erro no codigo
ini_set('display_errors', 1);

ini_set('display_startup_errors', 1);

error_reporting(E_ALL);
//FIM nao usar em produção

//VETOR = COLOCAR TODAS AS VARIAVEIS EM UM UNICO.

$despesas[0] = 354.55;
$despesas[1] = 135.00;
$despesas[2] = 600.00;
$despesas[3] = 900.00;
$despesas[4] = 400.00;

for ($i = 0; $i < 5; $i++) {

    echo $despesas[$i] . "<br>" ; 
}

unset($despesas); // apaga/destrói o vetor

$despesas['mercado'] = 354.55;
$despesas['estacionamento'] = 135.00;
$despesas['alimentacao'] = 600.00;
$despesas['bar'] = 900.00;
$despesas['educacao'] = 400.00;

echo "<br>Despesas<br>";

foreach ($despesas as $nome => $gasto) // valor está associado ao indice que é o nome da despesa

echo "<br>$nome: R$ " . number_format($gasto, 2, ',', ',' . "<br><br>");


